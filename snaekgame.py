import pygame
import random

GW = 800
GH = 600
apple_size = 20
snake_speed= 10
FPS = 60

white = (255, 255, 255)
black = (0, 0, 0)
grey = (20, 20, 20)
red = (255, 0, 0)
green = (32, 136, 5)
blue = (3, 103, 255)

cubecolors = [(46, 233, 255), (46, 51, 165), (255, 65, 213), (255, 46, 46), (251, 253, 47), (46, 194, 96)]

clock = pygame.time.Clock()
gameDisplay = pygame.display.set_mode((GW, GH))
pygame.display.set_caption('Snaek')

class SnakeCube:
    def __init__(self, pos_x, pos_y, width, height, speed, color):
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.color = color
        self.width = width
        self.height = height
        self.speed = speed
        self.change_x = 0
        self.change_y = 0
        self.direction = 0

class TurningPoint:
    def __init__(self, pos_x, pos_y, direction):
        self.pos_x = pos_x
        self.pos_y = pos_y
        self.direction = direction

def snake_init():
    global turns
    global cubes
    turns = []
    cubes = []

def snake_on_apple():
    if cubes[0].pos_x + cubes[0].width >= apple_pos_x and cubes[0].pos_x <= apple_pos_x + apple_size and cubes[0].pos_y + cubes[0].height >= apple_pos_y and cubes[0].pos_y <= apple_pos_y + apple_size :
        return True
    else:
        return False

def snake_on_wall():
    if cubes[0].pos_x == 0 or cubes[0].pos_x == GW - cubes[0].width or cubes[0].pos_y == 0 or cubes[0].pos_y == GH - cubes[0].height:
        return True
    else:
        return False

def snake_on_snake():
    for cube in cubes:
        if cube == cubes[0]:
            return False
        elif cubes[0].pos_x >= cube.pos_x and cubes[0].pos_x <= cube.pos_x + cubes[0].width and cubes[0].pos_y >= cube.pos_y and cubes[0].pos_y <= cube.pos_y + cubes[0].height:
            return True
        else:
            return False
    
def generate_new_apple():
    global apple_pos_x 
    apple_pos_x = random.randint(30, GW - apple_size - 30)
    global apple_pos_y
    apple_pos_y = random.randint(30, GH - apple_size - 30)

def add_cube():
    if cubes[len(cubes) - 1].direction == 12:
        pos_y = cubes[len(cubes) - 1].pos_y + cubes[len(cubes) - 1].height
        pos_x = cubes[len(cubes) - 1].pos_x
        direction = cubes[len(cubes) - 1].direction
    elif cubes[len(cubes) - 1].direction == 3:
        pos_x = cubes[len(cubes) - 1].pos_x  - cubes[len(cubes) - 1].width
        pos_y = cubes[len(cubes) - 1].pos_y
        direction = cubes[len(cubes) - 1].direction
    elif cubes[len(cubes) - 1].direction == 6:
        pos_y = cubes[len(cubes) - 1].pos_y - cubes[len(cubes) - 1].height
        pos_x = cubes[len(cubes) - 1].pos_x
        direction = cubes[len(cubes) - 1].direction
    else:
        pos_x = cubes[len(cubes) - 1].pos_x + cubes[len(cubes) - 1].width
        pos_y = cubes[len(cubes) - 1].pos_y
        direction = cubes[len(cubes) - 1].direction

    cubes.append(SnakeCube(pos_x, pos_y, cubes[len(cubes) - 1].width, cubes[len(cubes) - 1].height, cubes[len(cubes) - 1].speed, cubes[len(cubes) - 1].color))
    cubes[len(cubes) -1].direction = cubes[len(cubes) -2].direction

def update_pos_cubes():
    i = len(turns)
    for cube in cubes:
        if i > 0:
            for turn in turns:
                if cube.pos_x == turn.pos_x and cube.pos_y == turn.pos_y:
                    cube.direction = turn.direction

                    if cube == cubes[len(cubes) - 1]:
                        del turns[0]
        if cube.direction == 12:
            cube.change_y = -cube.speed
            cube.change_x = 0
        elif cube.direction == 3:
            cube.change_y = 0

            cube.change_x = cube.speed
        elif cube.direction == 6:
            cube.change_y = cube.speed
            cube.change_x = 0
        elif cube.direction == 9:
            cube.change_y = 0
            cube.change_x = -cube.speed

        cube.pos_x += cube.change_x
        cube.pos_y += cube.change_y
        pygame.draw.rect(gameDisplay, cube.color, [cube.pos_x, cube.pos_y, cube.width, cube.height])

def text_display(text, size, x, y):
   font=pygame.font.Font("hobgoblin.otf", size)
   scoretext=font.render(text, 1,(255,255,255))
   gameDisplay.blit(scoretext, (x, y))

def add_highscore(text):
    file = open("highscores.txt", "r")
    current = file.read()
    print current
    file = open("highscores.txt", "w")
    file.write(str(current) + "\n" + str(text))
    file.close()

def read_highscore():
    file = open ("highscores.txt", "r")
    return file.read()








def game_screen():

    gameExit = False
    global score
    score = 0
    snake_init()

    cubes.append(SnakeCube(400, 300, 10, 10, snake_speed, white))
    generate_new_apple()

    while not gameExit:

        if snake_on_apple():
            generate_new_apple()
            add_cube()
            score += 1
            print score

        if snake_on_wall():
            gameExit = True

        if snake_on_snake():
            gameExit = True

        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                gameExit = True

            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    gameExit = True
                if event.key == pygame.K_LEFT and cubes[0].direction != 3:
                    turns.append(TurningPoint(cubes[0].pos_x, cubes[0].pos_y, 9))
                    cubes[0].direction = 9
                if event.key == pygame.K_RIGHT and cubes[0].direction != 9:
                    turns.append(TurningPoint(cubes[0].pos_x, cubes[0].pos_y, 3))
                    cubes[0].direction = 3
                if event.key == pygame.K_UP and cubes[0].direction != 6:
                    turns.append(TurningPoint(cubes[0].pos_x, cubes[0].pos_y, 12))
                    cubes[0].direction = 12
                if event.key == pygame.K_DOWN and cubes[0].direction != 12:
                    turns.append(TurningPoint(cubes[0].pos_x, cubes[0].pos_y, 6))
                    cubes[0].direction = 6

        gameDisplay.fill(grey)
        text_display("Score: " + str(score), 20, 10, 10)
        pygame.draw.rect(gameDisplay, red, [apple_pos_x, apple_pos_y, apple_size, apple_size])
        update_pos_cubes()
        pygame.display.update()
        clock.tick(FPS)

def death_screen():

    end = False

    logo = pygame.image.load("snaek.png")
    add_highscore(str(score) + " awdawd")

    while not end:

        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                end = True

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    end = True
                if event.key == pygame.K_SPACE:
                    game_screen()
                if event.key == pygame.K_RETURN:
                    highscores_screen()

            gameDisplay.fill(grey)
            gameDisplay.blit(logo, (200,100))
            text_display("Game Over", 40, 310, 250)
            text_display("Score: " + str(score), 30, 350, 300)
            text_display("( Press space to play again )", 20, 280, 350)
            text_display("( Press enter to view highscores )", 20, 260, 380)
            pygame.display.update()  

def highscores_screen():

    end = False

    logo = pygame.image.load("snaek.png")

    while not end:

        for event in pygame.event.get():

            if event.type == pygame.QUIT:
                end = True

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    end = True
                if event.key == pygame.K_SPACE:
                    end = True

        gameDisplay.fill(black)
        text_display(read_highscore(), 20, 10, 10)
        pygame.display.update()


