import pygame
import random
import snaekgame

pygame.init()

white = (255, 255, 255)
black = (20, 20, 20)
grey = (20, 20, 20)

GW = 800 
GH = 600
 
logo = pygame.image.load("snaek.png")

window = pygame.display.set_mode((GW, GH))
pygame.display.set_caption('Snaek')

gameExit = False

if __name__ == '__main__':

    while not gameExit:

        window.fill(grey)  
        window.blit(logo, (200,100))
        snaekgame.text_display("Welcome to Snaek", 40, 250, 250)
        snaekgame.text_display("( Press space to start the game )", 20, 260, 320)
        pygame.display.update()  

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                gameExit = True
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    gameExit = True
                if event.key == pygame.K_SPACE:
                    print "Loading game_screen function"
                    snaekgame.game_screen()
                    print "Loading death_screen function"
                    snaekgame.death_screen()


    pygame.quit()
    quit()  